// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMELIFT_UE4_GameLift_UE4Character_generated_h
#error "GameLift_UE4Character.generated.h already included, missing '#pragma once' in GameLift_UE4Character.h"
#endif
#define GAMELIFT_UE4_GameLift_UE4Character_generated_h

#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_SPARSE_DATA
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_RPC_WRAPPERS
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGameLift_UE4Character(); \
	friend struct Z_Construct_UClass_AGameLift_UE4Character_Statics; \
public: \
	DECLARE_CLASS(AGameLift_UE4Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameLift_UE4"), NO_API) \
	DECLARE_SERIALIZER(AGameLift_UE4Character)


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGameLift_UE4Character(); \
	friend struct Z_Construct_UClass_AGameLift_UE4Character_Statics; \
public: \
	DECLARE_CLASS(AGameLift_UE4Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameLift_UE4"), NO_API) \
	DECLARE_SERIALIZER(AGameLift_UE4Character)


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGameLift_UE4Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGameLift_UE4Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGameLift_UE4Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGameLift_UE4Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGameLift_UE4Character(AGameLift_UE4Character&&); \
	NO_API AGameLift_UE4Character(const AGameLift_UE4Character&); \
public:


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGameLift_UE4Character(AGameLift_UE4Character&&); \
	NO_API AGameLift_UE4Character(const AGameLift_UE4Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGameLift_UE4Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGameLift_UE4Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGameLift_UE4Character)


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AGameLift_UE4Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AGameLift_UE4Character, FollowCamera); }


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_9_PROLOG
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_PRIVATE_PROPERTY_OFFSET \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_SPARSE_DATA \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_RPC_WRAPPERS \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_INCLASS \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_PRIVATE_PROPERTY_OFFSET \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_SPARSE_DATA \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_INCLASS_NO_PURE_DECLS \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMELIFT_UE4_API UClass* StaticClass<class AGameLift_UE4Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GameLift_UE4_Source_GameLift_UE4_GameLift_UE4Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
