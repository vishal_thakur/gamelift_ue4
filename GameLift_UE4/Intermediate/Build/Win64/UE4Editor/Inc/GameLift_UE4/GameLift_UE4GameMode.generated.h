// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMELIFT_UE4_GameLift_UE4GameMode_generated_h
#error "GameLift_UE4GameMode.generated.h already included, missing '#pragma once' in GameLift_UE4GameMode.h"
#endif
#define GAMELIFT_UE4_GameLift_UE4GameMode_generated_h

#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_SPARSE_DATA
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_RPC_WRAPPERS
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGameLift_UE4GameMode(); \
	friend struct Z_Construct_UClass_AGameLift_UE4GameMode_Statics; \
public: \
	DECLARE_CLASS(AGameLift_UE4GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameLift_UE4"), GAMELIFT_UE4_API) \
	DECLARE_SERIALIZER(AGameLift_UE4GameMode)


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGameLift_UE4GameMode(); \
	friend struct Z_Construct_UClass_AGameLift_UE4GameMode_Statics; \
public: \
	DECLARE_CLASS(AGameLift_UE4GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GameLift_UE4"), GAMELIFT_UE4_API) \
	DECLARE_SERIALIZER(AGameLift_UE4GameMode)


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMELIFT_UE4_API AGameLift_UE4GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGameLift_UE4GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMELIFT_UE4_API, AGameLift_UE4GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGameLift_UE4GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMELIFT_UE4_API AGameLift_UE4GameMode(AGameLift_UE4GameMode&&); \
	GAMELIFT_UE4_API AGameLift_UE4GameMode(const AGameLift_UE4GameMode&); \
public:


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMELIFT_UE4_API AGameLift_UE4GameMode(AGameLift_UE4GameMode&&); \
	GAMELIFT_UE4_API AGameLift_UE4GameMode(const AGameLift_UE4GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMELIFT_UE4_API, AGameLift_UE4GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGameLift_UE4GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGameLift_UE4GameMode)


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_9_PROLOG
#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_SPARSE_DATA \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_RPC_WRAPPERS \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_INCLASS \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_SPARSE_DATA \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_INCLASS_NO_PURE_DECLS \
	GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMELIFT_UE4_API UClass* StaticClass<class AGameLift_UE4GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GameLift_UE4_Source_GameLift_UE4_GameLift_UE4GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
